- R0 : le nombre de cafés, pour toutes les dates



select 
{[Measures].[Nb Café] }on columns,
{[Temps].allmembers} on rows
from Consommation

- R1 : le nombre de cafés et le nombre de chocolats par année

select 
{[Measures].[Nb Café], [Measures].[Nb Chocolat] }on columns,
{[Temps].[Libelle Annee].members} on rows
from Consommation

- R2 : le nombre de cafés par bâtiment et par an

select 
{ [Localisation].[Libelle Batiment].members  } on columns,
{[Temps].[Libelle Annee].members }on rows
from Consommation
where [Measures].[Nb Café] 


- R3 : le nombre de chocolats par bâtiment et par an

select 
{ [Localisation].[Libelle Batiment].members  } on columns,
{[Temps].[Libelle Annee].members }on rows
from Consommation
where [Measures].[Nb Chocolat]

- R4 : le nombre de cafés par distributeur du bâtiment Blaise Pascal et par an

select 
{[Localisation].[Tous les Localisation].[Blaise Pascal].children } on columns,
{[Temps].[Libelle Annee].members }on rows
from Consommation
where [Measures].[Nb Chocolat]

- R5 : le nombre de cafés par an pour le distributeur « salle à café IF »


select 
{[Localisation].[Tous les Localisation].[Blaise Pascal].[salle à café IF]} on columns,
{[Temps].[Libelle Annee].members }on rows
from Consommation
where [Measures].[Nb Chocolat]

- R6 : le nombre de cafés par bâtiment, par distributeur et par an


select 
{ [Localisation].members} on columns,
{[Temps].[Libelle Annee].members }on rows
from Consommation
where [Measures].[Nb Café]


select 
{ [Localisation].[Libelle Batiment].members, [Localisation].[Libelle Machine].members} on columns,
{[Temps].[Libelle Annee].members }on rows
from Consommation
where [Measures].[Nb Café]


- R7 : le nombre de cafés par bâtiment et par mois pour l’année 2015

select 
{ [Localisation].[Libelle Batiment].members  } on columns,
{[Temps].[Tous les Temps].[2015].children }on rows
from Consommation
where [Measures].[Nb Café] 

- R8 : la différence entre le nombre de cafés à l’instant t et le nombre de cafés à
l’instant t-1

with MEMBER [Measures].[PrevCafe] as ' [Measures].[Nb Café] - ([Measures].[Nb Café], [Temps].CurrentMember.PrevMember)  '
select 
{[Measures].allmembers}on columns,
{[Temps].allmembers} on rows
from Consommation


- R9 : le nombre de cafés et le nombre de chocolats par année (en colonne), pour
chaque localisation (en ligne)


select 
Crossjoin([Temps].[Libelle Annee].members , {[Measures].[Nb Café],[Measures].[Nb Chocolat]}  ) on columns,
{ [Localisation].members }on rows
from Consommation


- R10 : le nombre de cafés vendus pour les 2 meilleurs jours de la semaine (utilisez une
dimension virtuelle sur les labels des jours, à créer dans Analysis Service)